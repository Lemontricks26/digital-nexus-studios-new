﻿using UnityEngine;
using System.Collections;

public class ObstacleDeath : MonoBehaviour {
    
	void Start () {
	
	}

	void Update () {
	   
	}

    void OnTriggerEnter(Collider col)
    {
        if (this.gameObject.tag == "WallDeath")
        {
			if (col.gameObject.name == "C1" || col.gameObject.name == "C2" && col.gameObject.name != "C3")
            {
                col.gameObject.GetComponent<CharacterSelect>().Alive = false;
            }
        }
        if(this.gameObject.tag == "DoorDeath")
        {
			if (col.gameObject.name == "C1" || col.gameObject.name == "C3" && col.gameObject.name != "C2")
            {
                col.gameObject.GetComponent<CharacterSelect>().Alive = false;
            }
        }
    }
}
