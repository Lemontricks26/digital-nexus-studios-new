﻿using UnityEngine;
using System.Collections;

public class Wall_Obstacle : MonoBehaviour {

    public GameObject main;
    public ParticleSystem particle;
    private AudioSource aud;
    private GameObject gameController;

    void Start()
    {
        aud = GameObject.FindGameObjectWithTag("WallSound").GetComponent<AudioSource>();
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    void OnTriggerEnter(Collider col)
    {
        // Detector
        if (col.gameObject.name == "Detector")
        {
            gameController.GetComponent<UI>().incWall.enabled = true;
        }
        // Wall obstacle collision
        if (col.gameObject.tag == "Tank" && col.gameObject.GetComponent<CharacterSelect>().markerSet == 1)
        {
            gameController.GetComponent<ScoreManagingScript>().AddPoints(100);
            Destroy(main);
            particle.Play();
            aud.Play();
            Destroy(this.gameObject);
        }
        else if (col.gameObject.tag == "Hacker" || col.gameObject.tag == "Builder" && col.gameObject.GetComponent<CharacterSelect>().markerSet != 1)
        {
            gameController.GetComponent<ScoreManagingScript>().paused = true;
            Destroy(this.gameObject);
        }
    }
}