﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FloorGen : MonoBehaviour {

    public GameObject startArea;
    public GameObject[] Prison; // 1
    public GameObject[] RoofTop; // 2
    public GameObject[] Sewers; // 3
    public GameObject[] transitions;

    public GameObject Characters;
    public GameObject gameManager;
    public int currentZone = 1;
    public int direction = 0; // 1 for Up, 2 for Down
    public bool doTransition = false;

    private int tileType = 0; // 0 for Path, 1 for Obstacle
    private bool doTransitionTwo = false;
    private Vector3 blockPosition;
    private Quaternion rotation = Quaternion.Euler(0, 0, 0);
    private Quaternion rotation2 = Quaternion.Euler(0, 180, 0);

    void Start()
    {
        bool once = true;
        direction = Random.Range(1, 2);

        for (int i = 0; i <= 36; i += 12)
        {
            if (once == true)
            {
                blockPosition = new Vector3(i, transform.position.y, transform.position.z);
                blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
                once = false;
            }
            Instantiate(Prison[5], blockPosition, rotation);
            blockPosition = new Vector3((int)blockPosition.x + 12, (int)blockPosition.y, (int)blockPosition.z);
        }
    }

    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown("w"))
        {
            doTransition = true;
            gameManager.GetComponent<EnviromentChange>().counter = 0;
            direction = 1;
        }
        if (Input.GetKeyDown("s"))
        {
            doTransition = true;
            gameManager.GetComponent<EnviromentChange>().counter = 0;
            direction = 2;
        }
#endif
    }

    int prisonTileNum()
    {
        int num = 0;

        if (tileType == 1) // If last tile was a obstacle then create a path
        {
            num = Random.Range(4, 9);
        }
        else if (tileType == 0) // If last tile was a path then create a path or obstacle
        {
            num = Random.Range(1, 9);
        }

        return num;
    }

    int roofTileNum()
    {
        int num = 0;

        if (tileType == 1) // If last tile was a obstacle then create a path
        {
            num = Random.Range(4, 9);
        }
        else if (tileType == 0) // If last tile was a path then create a path or obstacle
        {
            num = Random.Range(1, 9);
        }

        return num;
    }

    int sewerTileNum()
    {
        int num = 0;

        if (tileType == 1) // If last tile was a obstacle then create a path
        {
            num = Random.Range(4, 8);
        }
        else if (tileType == 0) // If last tile was a path then create a path or obstacle
        {
            num = Random.Range(1, 8);
        }

        return num;
    }

    public void floorSelect()
    {
        if (doTransitionTwo == true)
        {
            changeZoneTwo();
        }
        else if (doTransition == true)
        {
            changeZone();
        }
        else
        {
            switch (currentZone)
            {
                case 1:
                    PrisonZone();
                    break;
                case 2:
                    RoofZone();
                    break;
                case 3:
                    SewerZone();
                    break;
            }
            GameObject.FindGameObjectWithTag("GameController").GetComponent<EnviromentChange>().counter += 1;
            blockPosition = new Vector3((int)blockPosition.x + 12, (int)blockPosition.y, (int)blockPosition.z);
        }
    }

    void changeZone()
    {
        if (currentZone == 1 && direction == 1)
        {
            Instantiate(transitions[0], blockPosition, rotation);
            // Prison to Roof Entry
        }
        else if (currentZone == 1 && direction == 2)
        {
            Instantiate(transitions[4], blockPosition, rotation);
            // Prison to Sewers Entry
        }
        else if (currentZone == 2 && direction == 1)
        {
            Instantiate(RoofTop[5], blockPosition, rotation2);
            // Roof to Roof
        }
        else if (currentZone == 2 && direction == 2)
        {
            Instantiate(transitions[2], blockPosition, rotation2);
            // Roof to Prison
        }
        else if (currentZone == 3 && direction == 1)
        {
            Instantiate(transitions[6], blockPosition, rotation);
            // Sewers to Prison
        }
        else if (currentZone == 3 && direction == 2)
        {
            Instantiate(Sewers[5], blockPosition, rotation2);
            // Sewers to Sewers
        }
        blockPosition.x += 12;
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
        tileType = 1;
        doTransition = false;
        doTransitionTwo = true;
    }

    void changeZoneTwo()
    {
        if (currentZone == 1 && direction == 1)
        {
            Instantiate(transitions[1], blockPosition, rotation);
            currentZone = 2;
            // Prison to Roof Exit
        }
        else if (currentZone == 1 && direction == 2)
        {
            Instantiate(transitions[5], blockPosition, rotation);
            currentZone = 3;
            // Prison to Sewers Exit
        }
        else if (currentZone == 2 && direction == 1)
        {
            Instantiate(RoofTop[5], blockPosition, rotation2);
            // Roof to Roof
        }
        else if (currentZone == 2 && direction == 2)
        {
            Instantiate(transitions[3], blockPosition, rotation);
            currentZone = 1;
            // Roof to Prison Exit
        }
        else if (currentZone == 3 && direction == 1)
        {
            Instantiate(transitions[7], blockPosition, rotation);
            currentZone = 1;
            // Sewers to Prison Exit
        }
        else if (currentZone == 3 && direction == 2)
        {
            Instantiate(Sewers[5], blockPosition, rotation2);
            // Sewers to Sewers
        }
        blockPosition.x += 12;
        blockPosition = new Vector3((int)blockPosition.x, (int)blockPosition.y, (int)blockPosition.z);
        tileType = 1;
        doTransitionTwo = false;
    }

    void PrisonZone()
    {
        switch (prisonTileNum())
        {
            case 1:
                // Door
                Instantiate(Prison[0], blockPosition, rotation);
                tileType = 1;
                break;
            case 2:
                // Pit
                Instantiate(Prison[1], blockPosition, rotation);
                tileType = 1;
                break;
            case 3:
                // Wall
                Instantiate(Prison[2], blockPosition, rotation);
                tileType = 1;
                break;
            case 4:
                // Path
                Instantiate(Prison[3], blockPosition, rotation);
                tileType = 0;
                break;
            case 5:
                // Path
                Instantiate(Prison[4], blockPosition, rotation);
                tileType = 0;
                break;
            case 6:
                // Path
                Instantiate(Prison[5], blockPosition, rotation);
                tileType = 0;
                break;
            case 7:
                // Path
                Instantiate(Prison[6], blockPosition, rotation);
                tileType = 0;
                break;
            case 8:
                // Path
                Instantiate(Prison[7], blockPosition, rotation);
                tileType = 0;
                break;
        }
    }

    void RoofZone()
    {
        switch (roofTileNum())
        {
            case 1:
                // Door
                Instantiate(RoofTop[0], new Vector3((int)blockPosition.x, 5.0135f, (int)blockPosition.z), rotation2);
                tileType = 1;
                break;
            case 2:
                // Pit
                Instantiate(RoofTop[1], blockPosition, rotation2);
                tileType = 1;
                break;
            case 3:
                // Wall
                Instantiate(RoofTop[2], blockPosition, rotation2);
                tileType = 1;
                break;
            case 4:
                // Path
                Instantiate(RoofTop[3], blockPosition, rotation2);
                tileType = 0;
                break;
            case 5:
                // Path
                Instantiate(RoofTop[4], blockPosition, rotation2);
                tileType = 0;
                break;
            case 6:
                // Path
                Instantiate(RoofTop[5], blockPosition, rotation2);
                tileType = 0;
                break;
            case 7:
                // Path
                Instantiate(RoofTop[6], blockPosition, rotation2);
                tileType = 0;
                break;
            case 8:
                // Path
                Instantiate(RoofTop[7], blockPosition, rotation2);
                tileType = 0;
                break;
        }
    }

    void SewerZone()
    {
        switch (sewerTileNum())
        {
            case 1:
                // Door
                Instantiate(Sewers[0], blockPosition, rotation2);
                tileType = 1;
                break;
            case 2:
                // Pit
                Instantiate(Sewers[1], blockPosition, rotation2);
                tileType = 1;
                break;
            case 3:
                // Wall
                Instantiate(Sewers[2], blockPosition, rotation2);
                tileType = 1;
                break;
            case 4:
                // Path
                Instantiate(Sewers[3], blockPosition, rotation2);
                tileType = 0;
                break;
            case 5:
                // Path
                Instantiate(Sewers[4], blockPosition, rotation2);
                tileType = 0;
                break;
            case 6:
                // Path
                Instantiate(Sewers[5], blockPosition, rotation2);
                tileType = 0;
                break;
            case 7:
                // Path
                Instantiate(Sewers[6], blockPosition, rotation2);
                tileType = 0;
                break;
        }
    }
}