﻿using UnityEngine;
using System.Collections;

public class EnviromentChange : MonoBehaviour {

    public FloorGen indicator;
    public int counter = 0;
    private bool doOnce = false;
    private int EnviroChange;

    void Awake()
    {
        Application.targetFrameRate = 60;
    }

    void Start ()
    {
        EnviroChange = Random.Range(10, 21);
    }
	
	void Update () {
        if (doOnce == true)
        {
            NewZone();
        }
        if(counter == EnviroChange)
        {
            EnviroChange = Random.Range(10, 21);
            doOnce = true;
        }
	}

    void NewZone()
    {
        if (indicator.currentZone == 1)
        {
            indicator.direction = Random.Range(1, 3);
        }
        else if (indicator.currentZone == 2)
        {
            indicator.direction = 2;
        }
        else if(indicator.currentZone == 3)
        {
            indicator.direction = 1;
        }
        
        indicator.doTransition = true;
        doOnce = false;
        counter = 0;
    }
}