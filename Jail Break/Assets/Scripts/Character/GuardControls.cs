﻿using UnityEngine;
using System.Collections;

public class GuardControls : MonoBehaviour {

    public GameObject playerMarker;
    public GameObject backMarker;
    public GameObject standbyMarker;
    public Collider standbyMarker_two;
    public GameObject guardWarning;

    public float forwardSpeed = 1;
    public float backwardsSpeed = 1;
    public int num;

    private float timerStandby;
    private float timer;

    Animator anim;

    private Collider coll;

    void Start () {
        coll = GetComponent<Collider>();
        timer = Random.Range(10f, 30f);
        timerStandby = 10;
        
        anim = transform.GetChild(0).GetComponent<Animator>();
    }

    void Update()
    {
        switch (num)
        {
            case 1:
                attack();
                break;
            case 2:
                standbye();
                break;
            case 3:
                retreat();
                break;
            case 4:
                recover();
                break;
        }

        if(standbyMarker_two.bounds.Contains(transform.position))
        {
            guardWarning.SetActive(true);
        }
        else
        {
            guardWarning.SetActive(false);
        }

#if UNITY_ANDROID || UNITY_IPHONE
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit hit;

            if (coll.Raycast(ray, out hit, 100.0f) && hit.transform.gameObject.name == this.gameObject.name)
            {
                num = 3;
                anim.SetBool("GOT CUCKT", true);
            }
        }
#endif
    }

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
    void OnMouseDown()
    {
        timer = 1.5f;
        num = 3;
    
        anim.SetBool("GOT CUCKT", true);
    }
#endif

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Builder" || col.gameObject.tag == "Tank" || col.gameObject.tag == "Hacker")
        {
            col.gameObject.GetComponent<CharacterSelect>().Alive = false;
        }
    }

    void standbye()
    {
        float step = forwardSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(standbyMarker.transform.position.x, 8, standbyMarker.transform.position.z), step);
        if (transform.position.x == standbyMarker.transform.position.x)
        {
            timerStandby -= Time.deltaTime;
            if (timerStandby < 0)
            {
                // Go to attack
                num = 1;
            }
        }
    }

    void attack()
    {
        float step = forwardSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(playerMarker.transform.position.x, 8, playerMarker.transform.position.z), step);
    }

    void retreat()
    {
        float step = forwardSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, backMarker.transform.position, step);
        if(transform.position.x == backMarker.transform.position.x)
        {
            // Go to recover
            timer = Random.Range(10f, 30f);
            num = 4;
        }
    }

    void recover()
    {
        anim.SetBool("GOT CUCKT", false);
        timer -= Time.deltaTime;
        if(timer < 0)
        {
            // Go to standbye
            timerStandby = 10;
            num = 2;
        }
    }
}