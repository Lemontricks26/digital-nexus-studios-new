﻿using UnityEngine;
using System.Collections;

public class destroyBackground : MonoBehaviour {

    GameObject obj;

	void Start () {
        obj = GameObject.FindGameObjectWithTag("Background");
	}

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Marker1")
        {
            Destroy(obj);
        }
    }
}