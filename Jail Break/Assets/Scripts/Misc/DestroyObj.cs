﻿using UnityEngine;
using System.Collections;

public class DestroyObj : MonoBehaviour {

    public GameObject obj;
    public GameObject inGameUI;
    public GameObject gameManager;
	public GameObject guard;
    public GameObject pauseButton;

	void Start () {
        inGameUI.SetActive(false);
        pauseButton.SetActive(false);
    }
	
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Tank" || col.gameObject.tag == "Builder" || col.gameObject.tag == "Hacker")
        {
            gameManager.GetComponent<ScoreManagingScript>().paused = false;
            gameManager.GetComponent<first_Time>().setHasPlayed(1);
            inGameUI.SetActive(true);
            Destroy(obj);
            pauseButton.SetActive(true);
            guard.SetActive(true);
            Destroy(this.gameObject);
        }
    }
}
