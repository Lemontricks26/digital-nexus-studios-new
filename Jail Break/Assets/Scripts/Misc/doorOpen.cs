﻿using UnityEngine;
using System.Collections;

public class doorOpen : MonoBehaviour {

	public Animator anim;

	void Start () {
	
	}
	
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Builder" || col.gameObject.tag == "Hacker" || col.gameObject.tag == "Tank") 
		{
			anim.enabled = true;
		}
	}
}