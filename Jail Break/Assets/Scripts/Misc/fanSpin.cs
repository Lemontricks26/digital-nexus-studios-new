﻿using UnityEngine;
using System.Collections;

public class fanSpin : MonoBehaviour {

	private int speed;

	// Use this for initialization
	void Start () {
		speed = Random.Range (100, 500);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.forward * Time.deltaTime * speed, Space.World);
	}
}
