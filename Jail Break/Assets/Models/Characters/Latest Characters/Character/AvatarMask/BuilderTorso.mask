%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: BuilderTorso
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: builder2
    m_Weight: 1
  - m_Path: CTRL_global
    m_Weight: 1
  - m_Path: CTRL_global/root_ctrl
    m_Weight: 1
  - m_Path: Reference_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/L_leg_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/L_leg_JNT/L_knee_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/L_leg_JNT/L_knee_JNT/L_ankle_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/L_leg_JNT/L_knee_JNT/L_ankle_JNT/L_ball_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/L_leg_JNT/L_knee_JNT/L_ankle_JNT/L_ball_JNT/L_toetip_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/R_leg_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/R_leg_JNT/R_knee_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/R_leg_JNT/R_knee_JNT/R_ankle_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/R_leg_JNT/R_knee_JNT/R_ankle_JNT/R_ball_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/R_leg_JNT/R_knee_JNT/R_ankle_JNT/R_ball_JNT/R_toetip_JNT
    m_Weight: 0
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/L_shoulder_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/L_shoulder_JNT/L_elbow_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/L_shoulder_JNT/L_elbow_JNT/L_wrist_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/L_shoulder_JNT/L_elbow_JNT/L_wrist_JNT/L_hand_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/L_shoulder_JNT/L_elbow_JNT/L_wrist_JNT/L_thumb_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/neck1_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/neck1_JNT/neck2_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/neck1_JNT/neck2_JNT/head_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/R_shoulder_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/R_shoulder_JNT/R_elbow_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/R_shoulder_JNT/R_elbow_JNT/R_wrist_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/R_shoulder_JNT/R_elbow_JNT/R_wrist_JNT/R_hand_JNT
    m_Weight: 1
  - m_Path: Reference_JNT/Root_JNT/hips_JNT/spine1_JNT/spine2_JNT/clavicle_JNT/R_shoulder_JNT/R_elbow_JNT/R_wrist_JNT/R_thumb_JNT
    m_Weight: 1
